# language: es

Característica: Entrega taller RESTful
Actividad intermedia taller RESTful

Escenario: Recuperar empresa teatral por nombre similar a "Big"
  Dado que existe la empresa teatral <"BigCo">
  Y que existe la empresa teatral <"TeatroCo">
  Cuando se solicita la empresa teatral con nombre similar a "Big"
  Entonces se obtiene la siguiente respuesta
      """
      {
         "StatusCode": 200,
         "StatusText": "Empresas teatrales recuperadas",
         "data": [
           {
              "id": 1,
              "name": "BigCo"
            }
          ]
      }
      """

Escenario: Recuperar empresa teatral por nombre similar a "Co"
  Dado que existe la empresa teatral <"BigCo">
  Y que existe la empresa teatral <"TeatroCo">
  Cuando se solicita la empresa teatral con nombre similar a "Co"
  Entonces se obtiene la siguiente respuesta
      """
      {
         "StatusCode": 200,
         "StatusText": "Empresas teatrales recuperadas",
         "data": [
           {
              "id": 1,
              "name": "BigCo"
           },
            {
              "id": 2,
              "name": "TeatroCo"
            }
            
          ]
      }
      """

Escenario: Recuperar empresa teatral por nombre similar a "Restful"
  Dado que existe la empresa teatral <"BigCo">
  Y que existe la empresa teatral <"TeatroCo">
  Cuando se solicita la empresa teatral con nombre similar a "Restful"
  Entonces se obtiene la siguiente respuesta
      """
      {
         "StatusCode": 200,
         "StatusText": "Empresas teatrales recuperadas",
         "data": []
      }
      """

Escenario: Recuperar los borderó paginados, página 1 con 5 elementos
  Dado que se cuenta con una lista de al menos 15 borderós
  Cuando se solicita la página 1 de borderós con 5 elementos
  Entonces se obtiene la página 1 con no más de 5 elementos
  Y no tiene página previa, 
  Y la página siguiente es 2
  Y la última página al menos 3
  
      # """
      # {
      #    "StatusCode": 200,
      #    "StatusText": "Pagina 1 de borderós recuperadas con éxito",
      #    "data": {
      #         current: 1;
      #         count: 3;
      #         prev: number;
      #         next: number;
      #         last: number;
      #         results: object[];
      #       }
      # }
      # """

  Escenario: Recuperar los borderó paginados, página 3 con 4 elementos
    Dado que se cuenta con una lista de al menos 15 borderós
    Cuando se solicita la página 3 de borderós con 4 elementos
    Entonces se obtiene la página 3 con no más de 4 elementos
    Y la página previa es 2, 
    Y la página siguiente es 4
    Y la última página al menos 4
    Y la cantidad total de elementos es al menos 15

  Escenario: Recuperar los borderó paginados, página 4 con 4 elementos
    Dado que se cuenta con una lista de al menos 15 borderós
    Cuando se solicita la página 4 de borderós con 4 elementos
    Entonces se obtiene la página 4 con no más de 4 elementos
    Y la página previa es 3, 
    Y no tiene página siguiente
    Y la última página al menos 4
    Y la cantidad total de elementos es al menos 15