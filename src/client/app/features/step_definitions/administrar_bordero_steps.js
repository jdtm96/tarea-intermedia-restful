const assert = require('assert');
const { Given, When, Then, AfterAll } = require('cucumber');
const request = require('sync-request');

const jsondiff = require('json-diff');

const url = 'http://server:8080/labprog-server/rest';

Given('que existe la empresa teatral <{string}>', function(aCustomer) {

   try {
      let res = request('GET', `${url}/customers/${aCustomer}`);

      let body = JSON.parse(res.getBody('utf8'));

      if (body.StatusCode == 200) {
         return assert.equal(aCustomer, body.data.name);
      } else {
         return assert.fail(body.StatusText);
      }
   } catch (error) {
      return assert.fail(error.message);
   }
});

Given('que se cuenta con la lista de obras', function(somePlays) {

   try {
      const res = request('GET', `${url}/plays/findAll`);

      const body = JSON.parse(res.getBody('utf8')); // cuerpo del texto utf8 parseado a JSON (contiene lo devuelto por findAll)
      const plays = JSON.parse(somePlays);


      if (body.StatusCode == 200) {
         return assert.equal(undefined, jsondiff.diff(plays, body));
      } else {
         return assert.fail(body.StatusText);
      }
   } catch (error) {
      return assert.fail(error.message);
   }
});


let response;

When('se ingresa el borderó', function (aBordero) {

  let res;

  try {
      res = request('POST', `${url}/borderos`, {
        json: JSON.parse(aBordero)
      });
      response = JSON.parse(res.getBody('utf8'));

      delete response.data["id"];

      if (response.StatusCode == 200) {
        return true;
      } else {
         return assert.fail(body.StatusText);
      }
    } catch (error) {
      return assert.fail(error.message);
  }
});

Then('se obtiene la siguiente respuesta', function (docString) {

  const json = JSON.parse(docString);

  return assert.equal(undefined, jsondiff.diff(json, response));
});

Given('la existencia de la base de datos de borderós', function() {
   return true;
});

When('se solicita la lista de borderós', function () {
  let res;

  try {
      res = request('GET', `${url}/borderos/findAll`);
      response = JSON.parse(res.getBody('utf8'));

      delete response.data["id"];

      if (response.StatusCode == 200) {
        return true;
      } else {
         return assert.fail(body.StatusText);
      }
    } catch (error) {
      return assert.fail(error.message);
  }
});

Then('se obtiene la lista de borderós no vacía', function () {

  return (response.data.length > 0)

});

Given('la existencia del borderó 2 de la compañía TeatroCo', function() {
  return true;
});

let bordero;

When('se solicta recuperar el borderó {int}', function (borderoId) {
  try {
    const res = request('GET', `${url}/borderos/${borderoId}`);

    response = JSON.parse(res.getBody('utf8'));

    if (response.StatusCode == 200) {
        bordero = response.data;
        return true;
    } else {
        return assert.fail(response.StatusText);
    }
  } catch (error) {
    return assert.fail(error.message);
  }
});


Given('el borderó {int} recuperado', function (borderoId) {
  return (bordero.id == borderoId);
});

Given('que se modifica la audiencia de {string} de {int} a {int}', function (code, audiencia, audienciaNew) {
  const idx = bordero.performances.findIndex(p => p.play.code === code);
  
  // Verifica que posea el valor original
  if (bordero.performances[idx].audience != audiencia)
  return false;
  
  // Asigna la nueva audiencia.
  bordero.performances[idx].audience = audienciaNew;

  return true;
});

When('se envía nuevamente el borderó modificado', function () {
  
  try {
    const res = request('PUT', `${url}/borderos`, {
      json: bordero
    });

    response = JSON.parse(res.getBody('utf8'));

    if (response.StatusCode == 200) {
      return true;
    } else {
       return assert.fail(body.StatusText);
    }
  } catch (error) {
    return assert.fail(error.message);
  }
});


When('se solicita eliminar el borderó {int}', function (borderoId) {

  try {
    const res = request('DELETE', `${url}/borderos/${borderoId}`);

    response = JSON.parse(res.getBody('utf8'));

    if (response.StatusCode == 200) {
      return true;
    } else {
       return assert.fail(response.StatusText);
    }
  } catch (error) {
    return assert.fail(error.message);
  }
});

AfterAll(function () {
  //const res = request('DELETE', `${url}/borderos/2`);
});

// TAREA INTERMEDIA SNIPPETS
// Notas:
// - se nos dieron 16 borderós para cargar en la base de datos, por lo tanto
// "Y la última página al menos 3" del primer escenario fallará, porque son 3 paginas de 5
// elementos y una de 1 elemento.
// - se entiende "Y la última página al menos 3" como "Y la última página tiene al menos 3 elementos"

When('se solicita la empresa teatral con nombre similar a {string}', function (string) {

  try {
    const res = request('GET', `${url}/customers/likeName/${string}`);

    response = JSON.parse(res.getBody('utf8'));
    //const customers = JSON.parse(string);

    if (response.StatusCode == 200) {
      return true;
    } else {
       return assert.fail(response.StatusText);
    }
  } catch (error) {
    return assert.fail(error.message);
}
});

Given('que se cuenta con una lista de al menos {int} borderós', function (borderos) {
  let res;

  try {
    // traigo todos los objetos borderó
    res = request('GET', `${url}/borderos/findAll`);

    let body = JSON.parse(res.getBody('utf8'));

    if (body.StatusCode == 200) {
      return assert.equal(true, body.data.length >= borderos);
    } else {
       return assert.fail(body.StatusText);
    }
  } catch (error) {
    return assert.fail(error.message);
  }
});

When('se solicita la página {int} de borderós con {int} elementos', function (page, elem) {
  let res;

  try {
      res = request('GET', `${url}/borderos?page=${page}&cant=${elem}`);
      response = JSON.parse(res.getBody('utf8'));

      if (response.StatusCode == 200) {
        return true;
      } else {
         return assert.fail(body.StatusText);
      }
    } catch (error) {
      return assert.fail(error.message);
  }
});

// aquí se debería compara que sea la página page y que no tenga más de elem elementos
Then('se obtiene la página {int} con no más de {int} elementos', function (page, elem) {

  try {
      if (response.StatusCode == 200) {
        return (assert.equal(response.data.current, page) &&
                assert.equal(true, response.data.results.length <= elem));
      } else {
         return assert.fail(body.StatusText);
      }
    } catch (error) {
      return assert.fail(error.message);
  }
});

Then('no tiene página previa,', function () {
  try {
    if (response.StatusCode == 200) {
      return (assert.equal(response.data.prev, response.data.current));
    } else {
       return assert.fail(body.StatusText);
    }
  } catch (error) {
    return assert.fail(error.message);
  }
})

Then('la página siguiente es {int}', function (nextPage) {
  try {
    if (response.StatusCode == 200) {
      return (assert.equal(response.data.next, nextPage));
    } else {
       return assert.fail(body.StatusText);
    }
  } catch (error) {
    return assert.fail(error.message);
  }
});

// la última página tiene al menos {int} elementos
Then('la última página al menos {int}', function (elem) {
  let res;

  try {
      // debería ir a la última página, de la cantidad de elementos de response
      res = request('GET', `${url}/borderos?page=${response.data.last}&cant=${response.data.results.length}`);
      response = JSON.parse(res.getBody('utf8'));

      if (response.StatusCode == 200) {
        return (assert.equal(true, response.data.results.length >= elem));
      } else {
         return assert.fail(body.StatusText);
      }
    } catch (error) {
      return assert.fail(error.message);
  }
});

Then('la cantidad total de elementos es al menos {int}', function (elem) {
  try {
    if (response.StatusCode == 200) {
      // verifico que la cantidad total de elementos paginados (count) sea mayor/igual a elem
      return assert.equal(true, response.data.count >= elem);
    } else {
       return assert.fail(response.StatusText);
    }
  } catch (error) {
    return assert.fail(error.message);
  }
});

Then('la página previa es {int},', function (prevPage) {
  try {
    if (response.StatusCode == 200) {
      return (assert.equal(response.data.prev, prevPage));
    } else {
       return assert.fail(body.StatusText);
    }
  } catch (error) {
    return assert.fail(error.message);
  }
});

Then('no tiene página siguiente', function () {
  try {
    if (response.StatusCode == 200) {
      return (assert.equal(response.data.current, response.data.next));
    } else {
       return assert.fail(body.StatusText);
    }
  } catch (error) {
    return assert.fail(error.message);
  }
});