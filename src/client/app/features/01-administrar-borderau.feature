# language: es

Característica: List View & Edit de la liquidación de borderó por período
Necesaria para print-the-bill

Escenario: Cargar borderó de "BigCo" de marzo de 2020
   Dado que existe la empresa teatral <"BigCo">
   Y que se cuenta con la lista de obras
      """
      {
         "StatusCode": 200,
         "StatusText": "Obras recuperadas con éxito",
         "data": [
              {
              "code": "hamlet",
              "name": "Hamlet",
              "type": "tragedy"
            },
            {
              "code": "as-like",
              "name": "As You Like It",
              "type": "comedy"
            },
            {
              "code": "othello",
              "name": "Othello",
              "type": "tragedy"
            }
          ]
      }
      """
   Cuando se ingresa el borderó
      """
      {
        "id": 1,
        "customer": {
            "name": "BigCo"
          },
        "date": "2020-03-01",
        "performances": [
          {
              "play":{
                "code": "hamlet"
              },
              "audience": 55
          },
          {
              "play": {
                "code": "as-like"
              },
              "audience": 35
          },
          {
              "play":{
                "code": "othello"
              },
              "audience": 40
          }
        ]
      }
      """
   Entonces se obtiene la siguiente respuesta
    # Plays va el objeto completo
      """
      {
        "StatusCode": 200,
        "StatusText": "Se cargó el borderó exitosamente",
        "data": {
          "date": "2020-03-01",
          "customer": {
            "id": 1,
            "name": "BigCo"
          },
          "performances": [
            {
              "audience": 55,
              "play": {
                "id": 1,
                "code": "hamlet",
                "name": "Hamlet",
                "type": "tragedy"
              }
            },
            {
              "audience": 35,
              "play": {
                "id": 2,
                "code": "as-like",
                "name": "As You Like It",
                "type": "comedy"
              }
            },
            {
              "audience": 40,
              "play": {
                "id": 3,
                "code": "othello",
                "name": "Othello",
                "type": "tragedy"
              }
            }
          ]
        }
      }
      """


Escenario: Cargar borderó de "TeatroCo" de abril de 2020
   Dado que existe la empresa teatral <"TeatroCo">
   Y que se cuenta con la lista de obras
      """
      {
         "StatusCode": 200,
         "StatusText": "Obras recuperadas con éxito",
         "data": [
              {
              "code": "hamlet",
              "name": "Hamlet",
              "type": "tragedy"
            },
            {
              "code": "as-like",
              "name": "As You Like It",
              "type": "comedy"
            },
            {
              "code": "othello",
              "name": "Othello",
              "type": "tragedy"
            }
          ]
      }
      """
   Cuando se ingresa el borderó
      """
      {
        "id": 2,
        "customer": {
            "name":"TeatroCo"
          },
        "date":"2020-04-01",
        "performances": [
            {
              "play":{
                "code": "as-like"
              },
              "audience": 63
            },
            {
              "play":{
                "code": "othello"
              },
              "audience": 24
            }
        ]
      }
      """
   Entonces se obtiene la siguiente respuesta
      """
      {
        "StatusCode": 200,
        "StatusText": "Se cargó el borderó exitosamente",
        "data": {
          "date": "2020-04-01",
          "customer": {
            "id": 2,
            "name": "TeatroCo"
          },
          "performances": [
            {
              "audience": 63,
              "play": {
                "id": 2,
                "code": "as-like",
                "name": "As You Like It",
                "type": "comedy"
              }
            },
            {
              "audience": 24,
              "play": {
                "id": 3,
                "code": "othello",
                "name": "Othello",
                "type": "tragedy"
              }
            }
          ]
        }
      }
      """

Escenario: Obtener la lista de borderós cargados
   Dada la existencia de la base de datos de borderós
   Cuando se solicita la lista de borderós
   Entonces se obtiene la lista de borderós no vacía


Escenario: Eliminar el borderó 1
   Dado la existencia de la base de datos de borderós
   Cuando se solicita eliminar el borderó 1
   Entonces se obtiene la siguiente respuesta
      """
      {
        "StatusCode": 200,
        "StatusText": "borderó eliminado exitosamente",
        "data":{}
      }
      """


Escenario: Obtener el borderó 2 de TeatroCo para editar
   Dada la existencia del borderó 2 de la compañía TeatroCo
   Cuando se solicta recuperar el borderó 2
   Entonces se obtiene la siguiente respuesta
      """
      {
        "StatusCode": 200,
        "StatusText": "Borderó recuperado con éxito",
        "data": {
          "id": 2,
          "date": "2020-04-01",
          "customer": {
            "id": 2,
            "name": "TeatroCo"
          },
          "performances": [
            {
              "audience": 63,
              "play": {
                "id": 2,
                "code": "as-like",
                "name": "As You Like It",
                "type": "comedy"
              }
            },
            {
              "audience": 24,
              "play": {
                "id": 3,
                "code": "othello",
                "name": "Othello",
                "type": "tragedy"
              }
            }
          ]
        }
      }
      """

Escenario: Modificar en el borderó 2 la audiencia de "as-like" de 63 a 67
  Dado el borderó 2 recuperado
  Y que se modifica la audiencia de "as-like" de 63 a 67
  Cuando se envía nuevamente el borderó modificado
  Entonces se obtiene la siguiente respuesta
      """
      {
        "StatusCode": 200,
        "StatusText": "Se cargó el borderó exitosamente",
        "data": {
          "id": 2,
          "date": "2020-04-01",
          "customer": {
            "id": 2,
            "name": "TeatroCo"
          },
          "performances": [
            {
              "audience": 67,
              "play": {
                "id": 2,
                "code": "as-like",
                "name": "As You Like It",
                "type": "comedy"
              }
            },
            {
              "audience": 24,
              "play": {
                "id": 3,
                "code": "othello",
                "name": "Othello",
                "type": "tragedy"
              }
            }
          ]
        }
      }
      """



Escenario: Obtener la lista de borderós cargados luego de eliminar
   Dada la existencia de la base de datos de borderós
   Cuando se solicita la lista de borderós
   Entonces se obtiene la siguiente respuesta
      """
      {
        "StatusCode": 200,
        "StatusText": "Borderós recuperados con éxito",
        "data": [
          {
            "id": 2,
            "date": "2020-04-01",
            "customer": {
              "id": 2,
              "name": "TeatroCo"
            },
            "performances": [
              {
                "audience": 67,
                "play": {
                  "id": 2,
                  "code": "as-like",
                  "name": "As You Like It",
                  "type": "comedy"
                }
              },
              {
                "audience": 24,
                "play": {
                  "id": 3,
                  "code": "othello",
                  "name": "Othello",
                  "type": "tragedy"
                }
              }
            ]
          }
        ]
      }
      """
