import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';  // rxjs maneja un paradigma basado en eventos, es "observable" // Los observables son una interfaz para manejar una variedad de operaciones asincrónicas comunes.
import { HttpClient } from "@angular/common/http";                                        
                                      
import { Play } from './plays/play';
//ya no se usa import { PLAYS } from './plays/mock-plays'; // inserta servicio plays, el mock
import { DataPackage } from "./data-package";

@Injectable({
  providedIn: 'root'
})
export class PlayService {  // se exporta PLayService para que sea utilizable por otros

  private playsUrl = 'rest/plays';  // URL to web api
  
  constructor(private http: HttpClient,) { }  // se exporta el constructor

  all(): Observable<DataPackage> {
    return this.http.get<DataPackage>(this.playsUrl);
  }


  get(code: string): Observable<DataPackage> {    // ahora se exporta el servicio que retorna el paquete con los datosd de una Play dado un code
    return this.http.get<DataPackage>(`${this.playsUrl}/${code}`);
  }
  
  // old
  // get(id: number): Observable<Play> {			// se exporta el servicio que retorna una Play dada una id
  //   return of (PLAYS.find(play => play.id === id)); // of "encapsula" en un Observer la Play encontrada en el find
  // }                                                 // ó, "of" Devuelve una instancia observable que entrega sincrónicamente los valores proporcionados como argumentos.

  // save(play: Play): Observable<Play> {
  //   // buscamos play que corresponde
  //   let formerPlay = PLAYS.find(formerPlay => formerPlay.id === play.id);
  //   // modificamos sus valores
  //   for(let prop in formerPlay){
  //     formerPlay[prop] = play[prop];
  //   }

  //   // devolvemos observable
  //   return of(formerPlay);
  // }

  save(play: Play): Observable<DataPackage> {
    return this.http[play.id ? 'put' : 'post']<DataPackage>(this.playsUrl, play);
  }

  remove(code: string): Observable<DataPackage> {
    return this.http['delete']<DataPackage>(`${this.playsUrl}/${code}`);
  }

  byPage(page: number, cant: number): Observable<DataPackage> {
    return this.http.get<DataPackage>(`${this.playsUrl}?page=${page}&cant=${cant}`);
  }

  searchTypes(text: string): Observable<DataPackage> {
    return this.http.get<DataPackage>(`${this.playsUrl}/types/${text}`);
  }
}
