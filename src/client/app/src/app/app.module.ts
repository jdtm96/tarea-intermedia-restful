
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalConfirm } from './modal-component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PlaysComponent } from './plays/plays.component';
import { PlaysDetailComponent } from './plays-detail/plays-detail.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";
import { BorderosComponent } from './borderos/borderos.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PlaysComponent,
    PlaysDetailComponent,
    NgbdModalConfirm,
    BorderosComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    NgbdModalConfirm
  ]
})
export class AppModule { }
