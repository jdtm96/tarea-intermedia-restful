import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PlaysComponent } from './plays/plays.component';
import { PlaysDetailComponent } from './plays-detail/plays-detail.component';
import { BorderosComponent } from './borderos/borderos.component';

const routes: Routes = [
  { path: '', component:HomeComponent },
  { path: 'plays', component: PlaysComponent},
  { path: 'plays/:code', component: PlaysDetailComponent },
  { path: 'borderos', component: BorderosComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
