import { Component, OnInit } from '@angular/core';
  import { ActivatedRoute } from '@angular/router';
  import { Location } from '@angular/common';
  import { Observable, of } from 'rxjs';
  import { catchError, debounceTime, distinctUntilChanged, tap, switchMap, map } from 'rxjs/operators';

  import { Play } from '../plays/play';
  import { PlayService }  from '../play.service';

  @Component({
    selector: 'app-plays-detail',
    templateUrl: './plays-detail.component.html',
    styleUrls: ['./plays-detail.component.css']
  })
  export class PlaysDetailComponent implements OnInit {

    play: Play;
    searching: boolean = false;
    searchFailed: boolean = false;

    constructor(
      private route: ActivatedRoute,
      private playService: PlayService,
      private location: Location
      ) {
    }

    ngOnInit() {
      this.get();
    }

    get(): void {
      const code = this.route.snapshot.paramMap.get('code');
      if(code === 'new'){
        this.play = <Play>{};
      } else {
        this.playService.get(code)
          .subscribe(dataPackage => this.play = <Play>dataPackage.data);
      }
    }

    goBack(): void {
      this.location.back();
    }

    // save(): void {
    //   this.playService.save(this.play).subscribe(play => {this.play = play; this.goBack();});
    // }


  save(): void {
    this.playService.save(this.play)
      .subscribe(dataPackage => {
        this.play = <Play>dataPackage.data; 
        this.goBack();
      });
  }

  searchType = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => this.searching = true),
      switchMap(term => term.length < 2 ? [] :
        this.playService.searchTypes(term)
        .pipe(
          map(response => response.data)
        )
        .pipe(
          tap(() => this.searchFailed = false),
          catchError(() => {
            this.searchFailed = true;
            return of([]);
          }))
      ),
      tap(() => this.searching = false)
    )
  }