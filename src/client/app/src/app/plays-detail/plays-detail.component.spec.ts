import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaysDetailComponent } from './plays-detail.component';

describe('PlaysDetailComponent', () => {
  let component: PlaysDetailComponent;
  let fixture: ComponentFixture<PlaysDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaysDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaysDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
