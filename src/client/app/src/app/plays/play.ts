export interface Play {
    id: number;

    code: string;

    name: string;
    
    type: string;
}