import { Component, OnInit } from '@angular/core';
//import { PLAYS } from './mock-plays';
import { Play } from './play';
import { PlayService } from '../play.service';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
  import { NgbdModalConfirm } from "../modal-component";
  import { ResultsPage } from "../results-page";

@Component({
  selector: 'app-plays',
  templateUrl: './plays.component.html',
  styleUrls: ['./plays.component.css']
})
export class PlaysComponent implements OnInit {
  
  resultsPage: ResultsPage = <ResultsPage>{};
    pages: number[];
    currentPage: number = 1;
  
  constructor(
    private playService: PlayService, // indico cuál será el playService a usar
    private _modalService: NgbModal
    ) { }

  ngOnInit() {
    this.getPlays();
  }

  getPlays(): void {
    this.playService.byPage(this.currentPage, 2).subscribe((dataPackage) => {
      this.resultsPage = <ResultsPage>dataPackage.data;
      this.pages = Array.from(Array(this.resultsPage.last).keys());
    });
  }

  remove(code: string): void {
    const modal = this._modalService.open(NgbdModalConfirm);
    const that = this;
    modal.result.then(
      function () {
        that.playService.remove(code).subscribe((dataPackage) => that.getPlays());
      },
      function () { }
    );
    modal.componentInstance.title = "Eliminar obra";
    modal.componentInstance.message = "¿Esta seguro de eliminar la obra?";
    modal.componentInstance.description =
      "Si elimina la obra no la podrá utilizar luego.";
  }

  showPage(pageId: number): void {
    if(!this.currentPage){
      this.currentPage = 1;
    }
    let page = pageId;
    if (pageId == -2) { // First
      page = 1;
    }
    if (pageId == -1) { // Previous
      page = this.currentPage > 1 ? this.currentPage -1 : this.currentPage;
    }
    if (pageId == -3) { // Next
      page = this.currentPage < this.resultsPage.last ? this.currentPage + 1 : this.currentPage;
    }
    if (pageId == -4) { // Last
      page = this.resultsPage.last;
    }
    if (pageId > 1 && this.pages.length >= pageId) { // Number
      page = this.pages[pageId - 1] + 1;
    }
    this.currentPage = page;
    this.getPlays();
  };
}
