package stateless;

import javax.ejb.EJB;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.NamedQuery;
import javax.persistence.NoResultException;

import java.util.stream.Collectors;

import java.util.Collection;
import java.util.Calendar;

import model.Bordero;
import model.Play;

import stateless.CustomerService;
import stateless.PlayService;

import javax.persistence.Query;

@Stateless
public class BorderoServiceBean implements BorderoService {

  @PersistenceContext(unitName = "print-the-bill")
  protected EntityManager em;

  // Inyecta el stateless de Customer
  @EJB
  private CustomerService customerService;

  // Inyecta el stateless de Play
  @EJB
  private PlayService playService;

  public Bordero create(Bordero bordero) {

      // Aquí completa los datos faltantes de Customer.
      bordero.setCustomer(
        customerService.findByName(bordero.getCustomer().getName())
        );

      // Aquí completa los datos faltantes de Play
      bordero.setPerformances(
        bordero.getPerformances()
             .stream()
             .map(perf -> {
               perf.setPlay(playService.findByCode(perf.getPlay().getCode()));
               return perf;
              })
             .collect(Collectors.toList())
      );

      // Aquí debe PERSISTIR en borderó en la base de datos
  em.persist(bordero); // <-- Aquí es donde le decimos a JPA que persista el dato.

  return bordero;
  }

  public Collection<Bordero> findAll() {
    return
    em.createQuery(
      "select e from Bordero e", Bordero.class)
      .getResultList();
  }

  public Bordero delete(int id) {

    Bordero bordero = em.find(Bordero.class, id);

    if (bordero != null)
      em.remove(bordero);

    return bordero;
  }

  public Bordero find(int id){
    return em.find(Bordero.class, id);
  }

  public Bordero update(Bordero bordero){
    // Si tuvieramos un proceso de validación, iría aquí.
    em.merge(bordero);

    return bordero;
  }

  public ResultsPage<Bordero> findByPage(Integer page, Integer cantPerPage){
		// Cuento el total de resultados
		Query countQuery = em
				.createQuery("SELECT COUNT(e.id) FROM " + Bordero.class.getSimpleName() + " e");
		// Pagino
		Query query = em.createQuery("FROM " + Bordero.class.getSimpleName() + " e");
		query.setMaxResults(cantPerPage);
		query.setFirstResult((page - 1) * cantPerPage);
		Integer count = ((Long) countQuery.getSingleResult()).intValue();
		Integer lastPage = (int) Math.ceil((double) count / (double) cantPerPage);

		// Armo respuesta
		ResultsPage<Bordero> resultPage = new ResultsPage<Bordero>(page, count, page > 1 ? page - 1 : page,
				page >= lastPage ? lastPage : page + 1, lastPage, query.getResultList());
		return resultPage;
	}
}