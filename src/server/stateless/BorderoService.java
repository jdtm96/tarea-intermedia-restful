package stateless;

import model.Bordero;

import java.util.Collection;
import java.util.Calendar;

public interface BorderoService {

    public Bordero create(Bordero bordero);
    public Collection<Bordero> findAll();
    public Bordero delete(int id);
    public Bordero find(int id);
    public Bordero update(Bordero bordero);
    public ResultsPage<Bordero> findByPage(Integer page, Integer cantPerPage);
}