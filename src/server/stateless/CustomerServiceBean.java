package stateless;
import javax.persistence.NoResultException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ejb.Stateless;
import model.Customer;
import java.util.Collection;
import javax.persistence.Query;

@Stateless
public class CustomerServiceBean implements CustomerService {

  @PersistenceContext(unitName = "print-the-bill")
  protected EntityManager em;

  public Customer findByName(String name) {
    try {
      return
      em.createQuery(
        "select c from Customer c where c.name = :name", Customer.class )
        .setParameter("name", name)
        .getSingleResult();
    } catch (NoResultException e) {
      return null;
    }
  }

  public Collection<Customer> findLikeName(String likeName){
    try {
      return
      em.createQuery(
        "select c from Customer c where c.name LIKE :likeName", Customer.class )
        .setParameter("likeName", "%" + likeName + "%")
        .getResultList();
    } catch (NoResultException e) {
      return null;
    }
  }
}