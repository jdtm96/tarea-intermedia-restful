package stateless;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.NamedQuery;
import javax.persistence.NoResultException;

import java.util.Collection;

import model.Play;
import javax.persistence.Query;

@Stateless
public class PlayServiceBean implements PlayService {

  @PersistenceContext(unitName = "print-the-bill")
  protected EntityManager em;

  public Collection<Play> findAll() {

    try {
      return em.createQuery("select e from Play e order by e.id", Play.class).getResultList();
    } catch (NoResultException e) {
      return null;
    }

  }

  public Play findByCode(String code) {
    try {
      return em.createQuery("select e from Play e where e.code = :code", Play.class)
        .setParameter("code", code)
        .getSingleResult();
    } catch (NoResultException e) {
      return null;
    }

  }

  @Override
    public Play update(Play play) {
      em.merge(play);
      return play;
    }

    @Override
    public Play create(Play play) {
      em.persist(play);
      return play;
    }

    @Override
  public void remove(String code) {
    Play play = findByCode(code);
    em.remove(play);
  }
   
  @Override
  public ResultsPage<Play> findByPage(Integer page, Integer cantPerPage) {
        // Cuento el total de resultados
        Query countQuery = em
                .createQuery("SELECT COUNT(e.id) FROM " + Play.class.getSimpleName() + " e");
        // Pagino
        Query query = em.createQuery("FROM " + Play.class.getSimpleName() + " e");
        query.setMaxResults(cantPerPage);
        query.setFirstResult((page - 1) * cantPerPage);
        Integer count = ((Long) countQuery.getSingleResult()).intValue();
        Integer lastPage = (int) Math.ceil((double) count / (double) cantPerPage);

        // Armo respuesta
        ResultsPage<Play> resultPage = new ResultsPage<Play>(page, count, page > 1 ? page - 1 : page,
                page > lastPage ? page + 1 : lastPage, lastPage, query.getResultList());
        return resultPage;
    }
}