package stateless;

import java.util.Collection;

import model.Play;

public interface PlayService {

    public Collection<Play> findAll();
    public Play findByCode(String code);
    public Play update(Play play);
    public Play create(Play play);
    public void remove(String code);
    public ResultsPage<Play> findByPage(Integer page, Integer cantPerPage);
}