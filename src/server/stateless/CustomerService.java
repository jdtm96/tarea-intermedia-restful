package stateless;

import model.Customer;
import java.util.Collection;

public interface CustomerService {
    public Customer findByName(String name);
    public Collection<Customer> findLikeName(String name);
}