package servlet;

import java.util.Collection;

import java.io.IOException;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import javax.ejb.EJB;

import stateless.BorderoService;
import model.Bordero;

import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.DELETE;

import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import servlet.ResponseMessage;

import javax.ws.rs.QueryParam;
  import javax.ws.rs.DefaultValue;
  import stateless.ResultsPage;

@Path("/borderos")
public class BorderoServlet {

  @EJB
  private BorderoService service;

  private ObjectMapper mapper; // Transforma JSON en Objetos y viceversa.

  public BorderoServlet() {
    mapper = new ObjectMapper();

    // Le provee el formateador de fechas.
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    mapper.setDateFormat(df);
  }

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public String create(String json) {

    Bordero bordero;
    String data;

    try {
      bordero = mapper.readValue(json, Bordero.class);  // lee el JSON

      bordero = service.create(bordero);

      data = mapper.writeValueAsString(bordero);

    } catch (JsonProcessingException e) {
      return ResponseMessage
        .message(502, "No se pudo dar formato a la salida", e.getMessage());
    } catch (IOException e) {
      return ResponseMessage
        .message(501, "Formato incorrecto en datos de entrada", e.getMessage());
    }

    return ResponseMessage.message(
      200,
      "Se cargó el borderó exitosamente",
      data
    );
  }

  @GET
  @Path("/findAll")
  @Produces(MediaType.APPLICATION_JSON)
  public String findAll() {
    Collection<Bordero> borderos = service.findAll();
    String data;
    try {  

      data = mapper.writeValueAsString(borderos);

    } catch (JsonProcessingException e) {
      return ResponseMessage
        .message(502, "No se pudo dar formato a la salida", e.getMessage());
    }

    return ResponseMessage.message(
      200,
      "Borderós recuperados con éxito",
      data
    );
  }

  // ...
  @DELETE
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public String delete(@PathParam("id") int id) {
    Bordero bordero = service.delete(id);

    if (bordero == null)
      return ResponseMessage.message(
        503,
        "No se pudo eliminar el borderó " + id
      );

    return ResponseMessage.message(
      200,
      "borderó eliminado exitosamente",
      "{}"
    );
  }

  @GET
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public String find(@PathParam("id") int id) {
    Bordero bordero = service.find(id);
    String data;
    try {  

      data = mapper.writeValueAsString(bordero);

    } catch (JsonProcessingException e) {
      return ResponseMessage
        .message(502, "No se pudo dar formato a la salida", e.getMessage());
    }

    if (bordero == null)
      return ResponseMessage.message(501, "No se encontró el borderó " + id);

    return ResponseMessage.message(
      200,
      "Borderó recuperado con éxito",
      data
    );
  }

  @PUT
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public String update(String json) {

    Bordero bordero;
    String data;

    try {
      bordero = mapper.readValue(json, Bordero.class);  // lee el JSON

      bordero = service.update(bordero);

      data = mapper.writeValueAsString(bordero);

    } catch (JsonProcessingException e) {
      return ResponseMessage
        .message(502, "No se pudo dar formato a la salida", e.getMessage());
    } catch (IOException e) {
      return ResponseMessage
        .message(501, "Formato incorrecto en datos de entrada", e.getMessage());
    }

    return ResponseMessage.message(
      200,
      "Se cargó el borderó exitosamente",
      data
    );
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String findByPage(@DefaultValue("1") @QueryParam("page") Integer page, @DefaultValue("10") @QueryParam("cant") Integer cant) {

    // Se modifica este método para que utilice el servicio
    ResultsPage<Bordero> resultsPage = service.findByPage(page, cant);

    // Se contruye el resultado en base a lo recuperado desde la capa de negocio.
    String data;

    try {

      data = mapper.writeValueAsString(resultsPage);

    } catch (JsonProcessingException e) {
      return ResponseMessage
        .message(502, "No se pudo dar formato a la salida", e.getMessage());
    } catch (IOException e) {
      return ResponseMessage
        .message(501, "Formato incorrecto en datos de entrada", e.getMessage());
    }

    return ResponseMessage.message(
      200,
      "Se recuperaron los borderós paginados",
      data
    );
  }

}