package servlet;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;


import servlet.ResponseMessage;

@Path("/serverStatus")
public class ServerStatus {

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String getServerStatus() {
    return ResponseMessage.message(200, "Server Online");
  }

}