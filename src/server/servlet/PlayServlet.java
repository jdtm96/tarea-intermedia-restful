package servlet;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import javax.ejb.EJB;
import java.util.Collection; // Se agrega el import de Collection
import java.util.stream.Collectors; // Se agrega el import de Collectors
import stateless.PlayService; // Se agrega el import del service

// a agregar
import javax.ws.rs.PUT;
import java.io.IOException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import javax.ws.rs.Consumes;
import java.text.SimpleDateFormat;

import servlet.ResponseMessage;

import model.Play;

import javax.ws.rs.POST;

import javax.ws.rs.DELETE;

import javax.ws.rs.QueryParam;
  import javax.ws.rs.DefaultValue;

  import stateless.ResultsPage;

  import java.util.regex.Pattern;
  import java.util.Arrays;
  import model.PerformanceType;

@Path("/plays")
public class PlayServlet {

  // Se agrega la inyección del servicio
  @EJB
  private PlayService service;

  // se agrega
  private ObjectMapper mapper; // Transforma JSON en Objetos y viceversa.

  // se cambia
  public PlayServlet(){
    mapper = new ObjectMapper();

    // Le provee el formateador de fechas.
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    mapper.setDateFormat(df);
  }

  //Comento esto porque no hace falta que traiga todos las obras
  //no necesito estos datos (findByPage ya nos sirve en lugar de getPlays)
  @GET
  @Path("/findAll")
  @Produces(MediaType.APPLICATION_JSON)
  public String getPlays() {

    // Se modifica este método para que utilice el servicio
    Collection<Play> plays = service.findAll();

    // Se contruye el resultado en base a lo recuperado desde la capa de negocio.
    String data = "[";

    data += plays.stream()
      .map(p -> "{\"code\":\"" + p.getCode() + "\",\"name\":\""+ p.getName() +"\",\"type\":\"" + p.getType() + "\"}")
      .collect(Collectors.joining(","));

    data += "]";

    return ResponseMessage.message(
      200,
      "Obras recuperadas con éxito",
      data
    );
  }

  @GET
  @Path("/{code}")
  @Produces(MediaType.APPLICATION_JSON)
  public String findByCode(@PathParam("code") String code) {

    // Se modifica este método para que utilice el servicio
    Play play = service.findByCode(code);

    // Se contruye el resultado en base a lo recuperado desde la capa de negocio.
    String data = play != null ?
                    "{\"code\":\"" + play.getCode() + "\",\"name\":\""+ play.getName() +"\",\"type\":\"" + play.getType() + "\", \"id\":"+play.getId()+"}"
                  :
                    "{}";

    return ResponseMessage.message(
      200,
      "Obras recuperadas con éxito",
      data
    );
  }

  // a agregar
  @PUT
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public String update(String json) {

    Play play;
    String data;

    try {
      play = mapper.readValue(json, Play.class);

      play = service.update(play);

      data = mapper.writeValueAsString(play);

    } catch (JsonProcessingException e) {
      return ResponseMessage
        .message(502, "No se pudo dar formato a la salida", e.getMessage());
    } catch (IOException e) {
      return ResponseMessage
        .message(501, "Formato incorrecto en datos de entrada", e.getMessage());
    }

    return ResponseMessage.message(
      200,
      "Se modificó correctamente la obra",
      data
    );
  }

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public String create(String json) {

    Play play;
    String data;

    try {
      play = mapper.readValue(json, Play.class);

      if(play.getCode().equals("new")){
        return ResponseMessage
        .message(503, "El código de la obra no puede ser \"new\".");
      }

      play = service.create(play);

      data = mapper.writeValueAsString(play);

    } catch (JsonProcessingException e) {
      return ResponseMessage
        .message(502, "No se pudo dar formato a la salida", e.getMessage());
    } catch (IOException e) {
      return ResponseMessage
        .message(501, "Formato incorrecto en datos de entrada", e.getMessage());
    }

    return ResponseMessage.message(
      200,
      "Se modificó correctamente la obra",
      data
    );
  }

  @DELETE
  @Path("/{code}")
  public String remove(@PathParam("code") String code) {
    service.remove(code);
    return ResponseMessage.message(
      200,
      "Se eliminó correctamente la obra"
    );
  }

  //@Path("/findByPage/")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String findByPage(@DefaultValue("1") @QueryParam("page") Integer page, @DefaultValue("10") @QueryParam("cant") Integer cant) {

    // Se modifica este método para que utilice el servicio
    ResultsPage<Play> resultsPage = service.findByPage(page, cant);

    // Se contruye el resultado en base a lo recuperado desde la capa de negocio.
    String data;

    try {

      data = mapper.writeValueAsString(resultsPage);

    } catch (JsonProcessingException e) {
      return ResponseMessage
        .message(502, "No se pudo dar formato a la salida", e.getMessage());
    } catch (IOException e) {
      return ResponseMessage
        .message(501, "Formato incorrecto en datos de entrada", e.getMessage());
    }

    return ResponseMessage.message(
      200,
      "Se recuperaron las obras paginadas",
      data
    );
  }

  @GET
  @Path("/types/{search}")
  @Produces(MediaType.APPLICATION_JSON)
  public String searchTypes(@PathParam("search") String typeSearch) {

    // Se contruye el resultado en base a lo recuperado desde la capa de negocio.
    String data;

    try {

      // pattern/expression to be match
      Pattern p = Pattern.compile(".*" + typeSearch + ".*", Pattern.CASE_INSENSITIVE);

      data = mapper.writeValueAsString(Arrays.stream(PerformanceType.values())
          .filter(x -> p.matcher(x.toString()).find()).collect(Collectors.toList()));

    } catch (IOException e) {
      return ResponseMessage.message(501, "Formato incorrecto en datos de entrada", e.getMessage());
    }

    return ResponseMessage.message(200, "Tipos de obras recuperados con éxito", data);
  }
}