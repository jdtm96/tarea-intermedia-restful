package servlet;

import javax.ejb.EJB;
import stateless.CustomerService;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import servlet.ResponseMessage;
import model.Customer;
import java.util.Collection;
import java.util.stream.Collectors;

@Path("/customers")
public class CustomerServlet {

  @EJB
  private CustomerService service;

  @GET
  @Path("/{name}")
  @Produces(MediaType.APPLICATION_JSON)
  public String getByName(@PathParam("name") String name) {
     Customer customer = service.findByName(name);

    if (customer == null)
      return ResponseMessage.message(502, "La companía " + name + " no existe.");

    return ResponseMessage.message(200, "Compañía recuperada con éxito", "{\"name\":\"" + customer.getName() + "\"}");
  }

  @GET
  @Path("/likeName/{likeName}")
  @Produces(MediaType.APPLICATION_JSON)
  public String getLikeName(@PathParam("likeName") String likeName) {
    Collection<Customer> customers = service.findLikeName(likeName);

    // if (customers == null)
    //   return ResponseMessage.message(502, "La companía " + likeName + " no existe.");

      //return ResponseMessage.message(200, "Empresas teatrales recuperadas", "{" + customers + "\"}");

      String data = "[";

      data += customers.stream()
        .map(c -> "{\"id\":" + c.getId() + ",\"name\":\""+ c.getName() + "\"}")
        .collect(Collectors.joining(","));

      data += "]";

      return ResponseMessage.message(
        200,
        "Empresas teatrales recuperadas",
        data
      );
    }
}