package model;

public enum PerformanceType {
    TRAGEDY, COMEDY;

    public String toDbValue() {
        return this.name().toLowerCase();
    }

    public static PerformanceType from(String performanceType) {
        // Note: error if null, error if not "ACTIVE" nor "INACTIVE"
        return PerformanceType.valueOf(performanceType.toUpperCase());
    }
}