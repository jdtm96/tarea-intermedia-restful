package model;

// import javax.persistence.Entity;
// import javax.persistence.Id;
// import javax.persistence.Column;
// import javax.persistence.GeneratedValue;
// import javax.persistence.GenerationType;
import javax.persistence.Embeddable;

// Agrega las relaciones
import javax.persistence.ManyToOne;

import model.Play;

//@Entity
@Embeddable
public class Performance {

  //@Id
  //@GeneratedValue(strategy = GenerationType.IDENTITY)
  //private int id;

  private int audience;

  // Hace referencia a la Play
  @ManyToOne
  private Play play;
  
  public Performance() {  }

  public int getAudience() {
    return audience;
  }

  public Play getPlay() {
    return play;
  }

  public void setAudience(int audience) {
    this.audience = audience;
  }

  public void setPlay(Play play) {
    this.play = play;
  }
}