package model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

// Agrega los tipos de datos para almacenar las fechas
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
// Agrega las facilidades para el mapeo de relaciones
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import java.util.Collection;
import java.util.Calendar;
import javax.persistence.ElementCollection;

import model.Customer;

//import javax.persistence.CascadeType;

@Entity
public class Bordero {

  @Id
  //@GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Temporal(TemporalType.DATE)
  private Calendar date;

  // Relaciona el borderó con el customer
  @ManyToOne
  private Customer customer;

  // Realciona el borderó con las performances
  
// ...
  // @OneToMany(cascade=CascadeType.PERSIST) // <-- Se reemplaza la relacion OneToMany por ElementCollection
  @ElementCollection(targetClass=Performance.class)  
  private Collection<Performance> performances;
  
  public Bordero() {  }

  public int getId() {
    return id;
  }

  public Calendar getDate() {
    return date;
  }

  public Customer getCustomer() {
    return customer;
  }

  public Collection<Performance> getPerformances() {
    return performances;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setDate(Calendar date) {
    this.date = date;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public void setPerformances(Collection<Performance> performances) {
    this.performances = performances;
  }
}