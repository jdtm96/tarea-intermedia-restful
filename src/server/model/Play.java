package model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
public class Play {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    private String code;

    @Column(unique = true)
    private String name;

    @Column(unique = false)
    private String type;

    public Play() { }

    public int getId() {
        return id;
    }
    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setCode(String code){
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    // @Override
    // public String toString() { 
    //     return "{\n\"code\": \"" + this.getCode() + "\",\n\"name\": \"" + this.getName() + "\",\n\"type\": \"" + this.getType() + "\"\n}";
    // }
}